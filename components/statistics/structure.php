<h2>Database structure overview</h2>
<table border="4">
  <tr>
    <th></th>
    <th>NAME</th>
    <th>PRIMARY</th>
    <th>TYPE</th>
    <th>NOT NULL</th>
    <TH>DEFAULT</th>
  </tr>
<?php
  $tables = $DB->SELECT->tableNames();
  foreach($tables as $table) {
    ?>
        <tr>
          <th><?= $table['name'] ?></th>
        </tr>
        <?php
          foreach($DB->SELECT->tableData($table['name']) as $key => $column) {
            echo "<tr>";
            echo "<td></td>";
            echo "<td>" . $column['name'] . "</td>";
            echo "<td>" . ($column['pk'] ? "true" : "false") . "</td>";
            echo "<td>" . $column['type'] . "</td>";
            echo "<td>" . ($column['notnull'] ? "true" : "false") . "</td>";
            echo "<td>" . $column['dflt_value'] . "</td>";
            echo "</tr>";
          }
        ?>
    <?php
  }
?>
</table>
