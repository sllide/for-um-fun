<?php
  function topic($id, $title, $creator, $firstPost, $date) {
    ?>
      <p>
        <h3><a href="?page=forum&amp;action=topic&amp;id=<?= $id ?>"><?= $title ?></a></h3>
        <p>
          <?= $firstPost ?>
        </p>
        Created by: <?= $creator ?> on <?= $date ?>
        <hr>
      </p>
    <?php
  }

  $topics = $DB->SELECT->topics($_GET['id']);
  if(count($topics)>0) {
    foreach($topics as $t) {
      $id = $t['rowid'];
      $title = $t['title'];
      $creator = ucwords($DB->SELECT->username($t['creatorID']));
      $firstPost = $DB->SELECT->firstPost($id)['content'];
      $stamp = $t['stamp'];
      topic($id, $title, $creator, $firstPost, $stamp);
    }
  } else {
    echo "No topics found! :(";
  }
?>
