<?php
  function catagory($id, $title, $desc, $count) {
    ?>
      <p>
        <h3><a href="?page=forum&amp;action=catagory&amp;id=<?= $id ?>"><?= $title ?></a></h3>
        <?= $desc ?><br />
        Number of topics: <?= $count ?>
        <hr>
      </p>
    <?php
  }

  foreach($DB->SELECT->catagories() as $c) {
    $id = $c['rowid'];
    $title = $c['title'];
    $desc = $c['description'];
    $count = $DB->SELECT->catagorySize($id);
    catagory($id, $title, $desc, $count);
  }
?>
