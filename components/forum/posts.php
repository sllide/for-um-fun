<?php
  function post($creator, $content, $date) {
    ?>
      <p>
        <h3><?= $creator ?> said on <?= $date ?></h3>
        <p>
          <?= $content ?>
        </p>
        <hr>
      </p>
    <?php
  }

  foreach($DB->SELECT->posts($_GET['id']) as $p) {
    $creator = ucwords($DB->SELECT->username($p['creatorID']));
    $content = $p['content'];
    $stamp = $p['stamp'];
    post($creator, $content, $stamp);
  }
?>
