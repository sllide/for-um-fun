<?php
//notice user about missing database
if(!$DB->doesExist) {
  require_once("pages/other/no_database.php");
} else { //if database exists resume normal execution
  //insert the page banner with navigation
  require_once('components/banner.php');

  //page loader logic
  if(isset($_GET['page'])) {
    $page = $_GET['page'];

    //default action to index to prevent invalid page insertions
    $action = "index";
    if(isset($_GET['action'])) { $action = $_GET['action']; }

    $url = getPage($page, $action);

    if($url) {
      require_once($url);
    } else {
      require_once('pages/other/denied.php');
    }
  } else {
    require_once('pages/other/home.php');
  }
}
?>
