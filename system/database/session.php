<?php
  //custom session handlers to have more control over sessions
  //this just to remove all session data when the database gets destroyed T.T
  class Session {

    private $dbo;

    function __construct($dbo) {
      $this->dbo = $dbo;
    }

    //this tells php if sessions can be used
    function open(){
      return $this->dbo->doesExist;
    }

    //this tells php if the database is closed, just return true.
    function close(){
      return true;
    }

    //get session data of id, return empty string if it doesnt exist
    function read($id){
      $data = $this->dbo->SELECT->session($id);
      if($data['data']) {
        return $data['data'];
      }
      return "";
    }

    //write/update session data with id
    function write($id, $data){
      $this->dbo->UPDATE->session($id, $data);
      return true;
    }

    //destroy session with id
    function destroy($id){
      $this->dbo->DELETE->session($id);
      return true;
    }

    //garbage collection
    function gc($treshold){
      $this->dbo->DELETE->expiredSessions($treshold);
      return true;
    }
  }
?>
