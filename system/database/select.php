<?php
  class Select {

    private $db;

    function __construct($db) {
      $this->db = $db;
    }

    //return all created table names
    function tableNames() {
      $q = $this->db->prepare("SELECT name FROM sqlite_master WHERE type='table'");
      $q->execute();
      return $q->fetchAll();
    }

    //return all created table names
    //for some reason this doesnt work with bindparam.
    function tableData($name) {
      $q = $this->db->prepare("PRAGMA table_info(".$name.")");
      //$q->bindParam(':name', $name);
      $q->execute();
      return $q->fetchAll();
    }

    //get all forum catagories
    function catagories() {
      $q = $this->db->prepare("SELECT ROWID, * FROM catagories");
      $q->execute();
      return $q->fetchAll();
    }

    //get all forum catagories
    function catagory($id) {
      $q = $this->db->prepare("SELECT * FROM catagories WHERE ROWID = :id");
      $q->bindParam(':id', $id);
      $q->execute();
      return $q->fetch();
    }

    function catagorySize($id) {
      $q = $this->db->prepare("SELECT COUNT(*) FROM topics WHERE catagoryID = :id");
      $q->bindParam(':id', $id);
      $q->execute();
      return $q->fetch()[0];
    }

    //return a user by name
    function userByName($name) {
      $q = $this->db->prepare("SELECT ROWID, * FROM users WHERE username = :name");
      $q->bindParam(':name', $name);
      $q->execute();
      return $q->fetch();
    }

    //return a user
    function user($id) {
      $q = $this->db->prepare("SELECT * FROM users WHERE ROWID = :id");
      $q->bindParam(':id', $id);
      $q->execute();
      return $q->fetch();
    }

    //return a username
    function username($id) {
      $q = $this->db->prepare("SELECT username FROM users WHERE ROWID = :id");
      $q->bindParam(':id', $id);
      $q->execute();
      return $q->fetch()['username'];
    }

    //get all topics linked to a catagory
    function topics($id) {
      $q = $this->db->prepare("SELECT ROWID, * FROM topics WHERE catagoryID = :id");
      $q->bindParam(':id', $id);
      $q->execute();
      return $q->fetchAll();
    }

    //get all topics linked to a catagory
    function posts($id) {
      $q = $this->db->prepare("SELECT * FROM posts WHERE topicID = :id");
      $q->bindParam(':id', $id);
      $q->execute();
      return $q->fetchAll();
    }

    //get all topics linked to a catagory
    function firstPost($id) {
      $q = $this->db->prepare("SELECT * FROM posts WHERE topicID = :id LIMIT 1");
      $q->bindParam(':id', $id);
      $q->execute();
      return $q->fetch();
    }

    //SESSION FUNCTION
    //get session data from a id
    function session($id) {
      $q = $this->db->prepare("SELECT data FROM sessions WHERE id = :id");
      $q->bindParam(':id', $id);
      $q->execute();
      return $q->fetch();
    }
  }
?>
