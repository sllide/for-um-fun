<?php

require_once('system/database/insert.php');
require_once('system/database/update.php');
require_once('system/database/select.php');
require_once('system/database/delete.php');
require_once('system/database/session.php');

class Database {

  private $db;
  public $doesExist;

  public $INSERT;
  public $UPDATE;
  public $SELECT;
  public $DELETE;
  public $SESSION;

  function __construct($connString) {
    //dont initialize the database if sqlite isnt installed/enabled.
    if(!extension_loaded('pdo_sqlite')) return;

    //initialize database
    $this->db = new PDO($connString);

    //enable error logging if asked for
    GLOBAL $CONFIG;
    if($CONFIG['debug']['showErrors']) {
      $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    $this->INSERT = new Insert($this->db);
    $this->UPDATE = new Update($this->db);
    $this->SELECT = new Select($this->db);
    $this->DELETE = new Delete($this->db);
    $this->SESSION = new Session($this);
    $this->doesExist = $this->dbExists();
  }

  function dbExists() {
    if(!$this->db) return false;
    global $CONFIG;
    //loop over all tables to see if all tables exist
    //This is something really cpu intensive when used in production, thank god it isnt ;)
    require('system/database/tables.php');
    foreach($DB_TABLES as $name => $value) {
      $q = $this->db->prepare("SELECT name FROM sqlite_master WHERE type='table' AND name=:name");
      $q->bindParam(':name', $name);
      $q->execute();
      if(!$q->fetch()) {
        return false;
      }
    }
    return true;
  }

  //Clear all tables from database
  function destroyDatabase() {
    if(!$this->db) return;
    foreach($this->SELECT->tableNames() as $table) {
      $q = $this->db->exec("DROP TABLE IF EXISTS $table[0]");
    }
  }

  //loop over all tables in $DB_TABLES and create them
  function buildDatabase() {
    if(!$this->db) return;
    require('system/database/tables.php');
    foreach($DB_TABLES as $name => $content) {
      //build query
      $query = "CREATE TABLE " . $name . "(";
      foreach($content as $cell => $attributes) {
        $query .= $cell . " " . $attributes . ", ";
      }
      //strip trailing ", " to prevent syntax errors in the query
      $query = substr($query,0,-2).")";
      $this->db->exec($query);
    }
  }

  //insert a bunch of stupid test data
  function buildTestData() {
    if(!$this->db) return;
    $this->INSERT->catagory('Introduce yourself!', 'you will get banned if you dont!');
    $names = require_once('system/database/testnames.php');
    foreach(array_rand($names, 100) as $id) {
      $uID = $this->INSERT->user($names[$id],$names[$id],1);
      $tID = $this->INSERT->topic("Hi, i'm " . $names[$id], $uID, 1);
      $this->INSERT->post("Nice to meet you all" . rand(0,100000000), $tID, $uID);
      $reactionCount = rand(3,15);
      for($i=0;$i<$reactionCount;$i++) {
        $this->INSERT->post("Hello " . $names[$id] . "!", $tID, rand(1,100));
      }
    }
  }
}
?>
