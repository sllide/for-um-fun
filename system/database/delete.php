<?php
  class Delete {

    private $db;

    function __construct($db) {
      $this->db = $db;
    }

    //remove all sessions from the database forcing a server wide logout
    function sessions() {
      $q = $this->db->prepare('DELETE FROM sessions');
      $q->execute();
    }

    //SESSION FUNCTION
    //deletes session with id
    function session($id) {
      $q = $this->db->prepare('DELETE FROM sessions WHERE id = :id');

      $q->bindParam(':id', $id);

      $q->execute();
      return true;
    }

    //SESSION FUNCTION
    //deletes all session rows thats havent been accessed for a certain time
    function expiredSessions($max) {
      $treshold = time() - $max;

      $q = $this->db->prepare('DELETE * FROM sessions WHERE access < :treshold');
      $q.bindParam(':treshold', $treshold);

      $q->execute();
      return true;
    }
  }
?>
