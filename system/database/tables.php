<?php
//each entry in the $tables array is an key wich represents the table name the
//value of this key is an array filled with the columns that belong in this table
  $DB_TABLES = Array(
    "users" => Array(
      'username'  => 'TEXT NOT NULL UNIQUE',
      'password'  => 'TEXT NOT NULL', //pasword hashed as MD5
      'level'     => 'INTEGER NOT NULL', //user permission level
      'stamp'     => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
    ),
    "catagories" => Array(
      'title'       => 'TEXT NOT NULL UNIQUE',
      'description' => 'TEXT NOT NULL',
      'stamp'       => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
    ),
    "topics" => Array(
      'title'       => 'TEXT NOT NULL',
      'creatorID'   => 'INTERGER NOT NULL', //id of user that posted this
      'catagoryID'  => 'INTERGER NOT NULL', //id of the catagory this belongs to
      'stamp'       => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
    ),
    "posts" => Array(
      'content'    => 'TEXT NOT NULL',
      'topicID'    => 'INTERGER NOT NULL', //id of the topic this post belongs to
      'creatorID'  => 'INTERGER NOT NULL', //id of the user that posted this
      'stamp'      => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
    ),
    "sessions" => Array(
      'id'        => 'TEXT PRIMARY KEY',
      'access'    => 'INTEGER DEFAULT NULL',
      'data'      => 'TEXT',
    ),
  );
?>
