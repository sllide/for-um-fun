<?php
  class Update {

    private $db;

    function __construct($db) {
      $this->db = $db;
    }

    //SESSION FUNCTION
    //updates data from certain id
    function session($id, $data) {
      $query = "REPLACE INTO sessions (id, access, data) VALUES (:id, :access, :data)";
      $data = Array(
        ':id' => $id,
        ':access' => time(),
        ':data' => $data,
      );
      $q = $this->db->prepare($query);
      $q->execute($data);
    }
  }
?>
