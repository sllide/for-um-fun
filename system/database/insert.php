<?php
  class Insert {

    private $db;

    function __construct($db) {
      $this->db = $db;
    }

    //insert a new user with a hashed password!
    function user($name, $password, $level) {
      $query = "INSERT INTO users (username, password, level) VALUES(:name, :password, :level)";
      $data = Array(
        ':name' => strtolower($name),
        ':password' => md5($password),
        ':level' => $level,
      );
      $q = $this->db->prepare($query);
      $q->execute($data);
      return $this->db->lastInsertId();
    }

    function catagory($title, $description) {
      $query = "INSERT INTO catagories (title, description) VALUES(:title, :description)";
      $data = Array(
        ':title' => swapTags($title),
        ':description' => swapTags($description),
      );
      $q = $this->db->prepare($query);
      $q->execute($data);
      return $this->db->lastInsertId();
    }

    function topic($title, $creator, $catagory) {
      $query = "INSERT INTO topics (title, creatorID, catagoryID) VALUES(:title, :creator, :catagory)";
      $data = Array(
        ':title' => swapTags($title),
        ':creator' => $creator,
        ':catagory' => $catagory,
      );
      $q = $this->db->prepare($query);
      $q->execute($data);
      return $this->db->lastInsertId();
    }

    function post($content, $topic, $creator) {
      $query = "INSERT INTO posts (content, topicID, creatorID) VALUES(:content, :topic, :creator)";
      $data = Array(
        ':content' => swapTags($content),
        ':topic' => $topic,
        ':creator' => $creator,
      );
      $q = $this->db->prepare($query);
      $q->execute($data);
      return $this->db->lastInsertId();
    }
  }
?>
