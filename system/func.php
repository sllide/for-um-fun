<?php
  //returns the correct page location given the user level and _GET parameters
  function getPage($page, $action) {
    $path = "pages/" . $page . "/" . $action . ".*.php";
    //check if page exists
    //checking with .*.php instead of .php ensures only pages with a level get found
    $result = glob($path);
    if(count($result)>0) {
      //if so check if user has a high enough level
      $url = $result[0];
      $levelNeeded = intval(explode('.', $url)[1]);

      if($_SESSION['level'] >= $levelNeeded) {
        if($levelNeeded != -1) {
          return $url;
        }
        if($_SESSION['level'] == 0) {
          return $url;
        }
      }
    } else {
      return "pages/other/404.php";
    }
    //return false if user has no access to page to make function more flexable
    return false;
  }

  function canAccessPage($page, $action) {
    if(getPage($page, $action)) {
      return true;
    }
    return false;
  }

  function swapTags($text) {
    $org = array("<", ">");
    $new = array("[", "]");
    return str_replace($org, $new, $text);
  }
?>
