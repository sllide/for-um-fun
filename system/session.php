<?php
  //session function takeover to be able to control sessions
  session_set_save_handler(
    Array($DB->SESSION, 'open'),
    Array($DB->SESSION, 'close'),
    Array($DB->SESSION, 'read'),
    Array($DB->SESSION, 'write'),
    Array($DB->SESSION, 'destroy'),
    Array($DB->SESSION, 'gc')
  );

  if($DB->doesExist) {
    session_start();

    //if session is empty fill it with nobody AKA logged out
    if(count($_SESSION) == 0) {
      $_SESSION['id'] = 0;
      $_SESSION['name'] = "nobody";
      $_SESSION['level'] = 0;
    }
  }
?>
