<?php

  if(isset($_POST['catagory']) && isset($_POST['title']) && isset($_POST['description'])) {
    $cID = $DB->INSERT->catagory($_POST['title'], $_POST['description']);
    header('location:/?page=forum&action=catagory&id=' . $cID);
  }

  //kill all active sessions
  if(isset($_GET['killSessions'])) {
    echo "killed all sessions.";
    $DB->DELETE->sessions();
    //destroy own session, else it gets rewritten to the database once php is done executing
    session_destroy();
    header("location:/");
  }

  //destroy the whole database
  if(isset($_GET['destroyDatabase'])) {
    $DB->destroyDatabase();
    header("location:/");
  }
?>
<h3>Quick options</h3>
<ul>
  <li><a href="/?page=admin&amp;killSessions">Destroy all sessions.</a></li>
  <li><a href="/?page=admin&amp;destroyDatabase">Destroy database.</a></li>
</ul>
<form method="POST">
  Create new catagory: <br />
  Title: <input type="text" name="title" required /><br />
  Description: <input type="text" name="description" required /><br />
  <input type="submit" name="catagory" value="create Catagory" />
</form>
