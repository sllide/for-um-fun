<?php
  //execute database magic
  if(isset($_POST['submit']) && isset($_POST['adminName']) && isset($_POST['adminPass'])) {
    $DB->destroyDatabase();
    $DB->buildDatabase();

    //insert admin before any other data to ensure admin has ID 1
    $DB->INSERT->user($_POST['adminName'], $_POST['adminPass'], 5);

    //insert prefab data needed to run the forum
    if(isset($_POST['testData'])) {
      $DB->buildTestData();
    }

    //redirect to homepage after building
    header("location:/");
  }
?>
<h1>Hi, welcome to <?= $CONFIG['general']['title'] ?>.</h1>
<h3>This page will help you setup the database required for this forum to function.</h3>
<font size="8"><font color="red">!</font>Please dont use this<font color="red">!</font></font>
<p>
  <?php
    //check if sqlite is installed
    if(!extension_loaded('pdo_sqlite')) { ?>
      You need the sqlite extention enabled for this forum to function. Ask your system admin to enable it or do it yourself if you dare.
    <?php } else { ?>
      <form method="POST">
        <table>
          <tr><th>Admin username</th><td><input type="text" name="adminName" required /></td></tr>
          <tr><th>Admin password</th><td><input type="text" name="adminPass" required /></td></tr>
          <tr><th>Create test data</th><td><input type="checkbox" name="testData" /> (Might take a while)</td></tr>
          <tr><th>Database</th><td><input type="submit" name="submit" value="Create" /></td></tr>
        </table>
      </form>
    <?php }
  ?>
</p>
