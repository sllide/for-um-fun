<?php
  if(isset($_POST['catagoryID']) && isset($_POST['title']) && isset($_POST['content'])) {
    $cID = $_POST['catagoryID'];
    $uID = $_SESSION['id'];
    $title = $_POST['title'];
    $content = $_POST['content'];
    //only create topic if catagory exists
    if($DB->SELECT->catagory($cID)) {
      $tID = $DB->INSERT->topic($title, $uID, $cID);
      $DB->INSERT->post($content, $tID, $uID);
      header('location:/?page=forum&action=topic&id=' . $tID);
    }
  }

  //stop execution if page doesnt exist
  if(!$DB->SELECT->catagory($_GET['id'])) {
    echo "Catagory doesnt exist.";
    return;
  }
?>
<h2>Create topic in catagory <?= $DB->SELECT->catagory($_GET['id'])['title'] ?></h2>
<form method="POST">
  <input type="hidden" name="catagoryID" value="<?= $_GET['id'] ?>" />
  <b>Title: </b> <input type="text" name="title" required /><br />
  <b>Content: </b><br />
  <textarea name="content" required></textarea><br />
  <input type="submit" value="Create topic"/>
</form>
