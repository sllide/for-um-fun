<?php
  //check if both username and password are set
  if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2'])) {
    $name = $_POST['username'];
    $pw1 = $_POST['password'];
    $pw2 = $_POST['password2'];

    //check if passwords match and username is letters only
    if($pw1 == $pw2 && ctype_alpha($name)) {
      $id = $DB->INSERT->user($_POST['username'],$_POST['password'],1);

      //if user creation is succesfull log him in and return to homepage
      $user = $DB->SELECT->user($id);
      if($user) {
        $_SESSION['id'] = $id;
        $_SESSION['name'] = ucwords($user['username']);
        $_SESSION['level'] = $user['level'];
        header('location:/');
      }
    }

    echo "Something went wrong!?";
  }
?>
<form method="post">
  <table>
    <tr><td>Username:</td><td><input type="text" name="username" required autofocus/></td></tr>
    <tr><td>Password:</td><td><input type="password"  name="password" required /></td></tr>
    <tr><td>Repeat password:</td><td><input type="password" name="password2" required /></td></tr>
    <tr><td><input type="submit" value="Login" /></td></tr>
  </table>
</form>
