<?php
  //Load config before all to ensure $CONFIG is available
  $CONFIG = parse_ini_file("system/config.ini", true, INI_SCANNER_TYPED);

  //load all misc functions
  require_once('system/func.php');

  //Require the database and make it accessable at $DB
  require_once('system/database/db.php');
  $DB = new Database($CONFIG['database']['connString']);

  //only require session if there actually is a session table available
  if($DB->doesExist) {
    require_once('system/session.php');
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <title><?= $CONFIG['general']['title'] ?></title>
  </head>
  <body>
    <?php require_once('system/page_loader.php'); ?>
  </body>
</html>
